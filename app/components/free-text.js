import Component from "@ember/component";

export default Component.extend({
  actions: {
    onEnter() {
      document.getElementById(this.nextQuestionId).scrollIntoView({
        behavior: "smooth",
        block: "center",
        inline: "end"
      });
    }
  }
});
