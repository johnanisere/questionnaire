import Component from '@ember/component';
import { computed } from '@ember/object';

export default Component.extend({
    questions: computed('questionnaire', function() {
        return this.get("questionnaire.questionnaire.questions")
    })
});