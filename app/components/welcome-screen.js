import Component from "@ember/component";
import { computed } from "@ember/object";

export default Component.extend({
  description: computed("question", function() {
    return this.get("question.questionnaire.description");
  }),

  actions: {
    proceed() {
      this.get("toggleStart")();
    }
  }
});
