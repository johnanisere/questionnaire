import Component from '@ember/component';
import { computed } from '@ember/object';

export default Component.extend({
    isMiultipleChoice: computed('question', function() {
        return this.get('question.question_type') === 'multiple-choice';
    }),
    isText: computed('question', function() {
        return this.get('question.question_type') === 'text';
    }),
    isMiultipleAnswer: computed('question', function() {
        return this.get('question.multiple') === true;
    }),
    choices: computed('question', function() {
        return this.get('question.choices');
    }),
    headline: computed('question', function() {
        return this.get('question.headline');
    }),

});