import Component from '@ember/component';
import { computed } from '@ember/object';

export default Component.extend({
    questionNumber: computed('index', function() {
        return this.get('index') + 1;
    }),

});