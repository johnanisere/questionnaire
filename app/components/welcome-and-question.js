import Component from "@ember/component";

export default Component.extend({
  start: false,
  actions: {
    toggleStart() {
      this.toggleProperty("start");
    }
  }
});
