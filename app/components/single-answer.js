import Component from "@ember/component";
import EmberObject from "@ember/object";

export default Component.extend({
  answer: EmberObject.create({}),
  answerIndex: null,
  actions: {
    setActiveAnswer(choice, index, headline) {
      if (this.answer.get(headline) === choice) {
        this.answer.set(headline, null);
        this.set("answerIndex", null);
      } else {
        this.answer.set(headline, choice);
        this.set("answerIndex", index);

        document.getElementById(this.nextQuestionId).scrollIntoView({
          behavior: "smooth",
          block: "center",
          inline: "end"
        });
      }
    }
  }
});
