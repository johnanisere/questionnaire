import Component from "@ember/component";
import { computed } from "@ember/object";

export default Component.extend({
  headline: computed("question", function() {
    return this.get("question.headline");
  }),
  questionId: computed("index", function() {
    return "index_" + this.get("index");
  }),
  nextQuestionId: computed("index", function() {
    return "index_" + (this.get("index") + 1);
  })
});
