import { helper } from "@ember/component/helper";

const numbering = ["A", "B", "C", "D", "E", "F", "G"];

export function listStyle(params) {
  return numbering[params];
}

export default helper(listStyle);
