import Route from "@ember/routing/route";
import questionnaire from "../utils/constants";

export default Route.extend({
  model() {
    return questionnaire;
  }
});
