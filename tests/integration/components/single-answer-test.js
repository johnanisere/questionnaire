import { module, test } from "qunit";
import { setupRenderingTest } from "ember-qunit";
import { render } from "@ember/test-helpers";
import hbs from "htmlbars-inline-precompile";

module("Integration | Component | single-answer", function(hooks) {
  setupRenderingTest(hooks);

  hooks.beforeEach(function() {
    this.choices = [
      {
        label: "Meine Familie mit Kindern",
        value: "Meine Familie mit Kindern",
        selected: false
      },
      {
        label: "Meine Familie ohne Kinder",
        value: "Meine Familie ohne Kinder",
        selected: false
      },
      {
        label: "Mich ohne Kind",
        value: "Mich ohne Kind",
        selected: false
      },
      {
        label: "Mich mit Kind",
        value: "Mich mit Kind",
        selected: false
      },
      {
        label: "Mich und meinen Lebenspartner",
        value: "Mich und meinen Lebenspartner",
        selected: false
      }
    ];
  });

  test("it renders all questions", async function(assert) {
    await render(hbs`{{single-answer choices=choices }}`);

    assert.equal(
      this.element.querySelector(".single-answer-title").textContent.trim(),
      "Meine Familie mit Kindern",
      "Each label corresponds to a question"
    );
  });
});
