import { module, test } from "qunit";
import { setupRenderingTest } from "ember-qunit";
import { render } from "@ember/test-helpers";
import hbs from "htmlbars-inline-precompile";

module("Integration | Component | question-tags", function(hooks) {
  setupRenderingTest(hooks);

  hooks.beforeEach(function() {
    (this.headline = "fake-question"), (this.index = 0);
  });

  test("should display question", async function(assert) {
    await render(hbs`{{question-tags headline=headline index=index}}`);
    assert.equal(
      this.element.querySelector(".question span").textContent.trim(),
      "fake-question",
      "headline: fake-question"
    );
    assert.equal(
      this.element.querySelector(".question-index span").textContent.trim(),
      "1",
      "index: 1"
    );
  });
});
