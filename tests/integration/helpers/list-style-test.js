import { module, test } from "qunit";
import { setupRenderingTest } from "ember-qunit";
import { render } from "@ember/test-helpers";
import hbs from "htmlbars-inline-precompile";

module("Integration | Helper | list-style", function(hooks) {
  setupRenderingTest(hooks);

  test("it renders correctly", async function(assert) {
    this.set("inputValue", "1");

    await render(hbs`{{list-style inputValue}}`);

    assert.equal(this.element.textContent.trim(), "B");
  });
});
